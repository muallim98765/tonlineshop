import { createApp } from 'vue'
import App from './App.vue'

import '@/assets/scss/main.scss';
import '@/assets/fonts/gotham/stylesheet.css';
import 'ant-design-vue/dist/reset.css';
import Antd from 'ant-design-vue';

const app = createApp(App);


// Own Components
import GButton from "@/components/global/button.vue"
import GText from "@/components/global/text.vue"

const OwnComponents = [
	{val: GButton, name: 'GButton'}, 
	{val: GText, name: 'GText'}, 
]
OwnComponents.forEach(component => app.component(component.name, component.val))
// Own Components

app.use(Antd);

app.mount('#app')
